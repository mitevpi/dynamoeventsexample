﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dynamo.Graph.Workspaces;
using Dynamo.ViewModels;
using Dynamo.Wpf.Extensions;

using DynamoEventsExample.EventWatcher.ViewModels;

namespace DynamoEventsExample
{
    class WorkspaceEventsExample
    {

        #region Properties

        private DynamoViewModel _dvm;
        private WorkspaceModel _cws;

        private EventWatcherViewModel _VM;

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance.
        /// </summary>
        /// <param name="p">ViewLoadedParams</param>
        public WorkspaceEventsExample(ViewLoadedParams p, EventWatcherViewModel vm)
        {
            // Store VM...
            _VM = vm;

            // Get DynamoViewModel from Dynamo Window Data Context...
            _dvm = p.DynamoWindow.DataContext as DynamoViewModel;

            // Get current Workspace from DynamoModel...
            _cws = _dvm.Model.CurrentWorkspace;

            // Subscribe to Workspace Added event so we know when we have opened an existing graph.
            _dvm.Model.WorkspaceAdded += Model_WorkspaceAdded;

            // Subsribe to Workspace Removed Event so we know when this graph is cleared.
            _dvm.Model.WorkspaceRemoved += Model_WorkspaceRemoved;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Subscribe to all Workspace Events.
        /// </summary>
        /// <param name="ws">Current Workspace</param>
        public void SubsribeToAllWorkspaceEvents(WorkspaceModel ws)
        {
            //Subscribe to Workspace Annotation Events...
            ws.AnnotationAdded += CurrentWorkspace_AnnotationAdded;
            ws.AnnotationRemoved += CurrentWorkspace_AnnotationRemoved;
            ws.AnnotationsCleared += CurrentWorkspace_AnnotationsCleared;

            //Subscribe to Workspace Node Events...
            ws.NodeAdded += CurrentWorkspace_NodeAdded;
            ws.NodeRemoved += CurrentWorkspace_NodeRemoved;
            ws.NodesCleared += CurrentWorkspace_NodesCleared;

            //Subscribe to Workspace Note Events...
            ws.NoteAdded += CurrentWorkspace_NoteAdded;
            ws.NoteRemoved += CurrentWorkspace_NoteRemoved;
            ws.NotesCleared += CurrentWorkspace_NotesCleared;

            //Subscribe to Workspace Connector Events...
            ws.ConnectorAdded += CurrentWorkspace_ConnectorAdded;
            ws.ConnectorDeleted += CurrentWorkspace_ConnectorDeleted;

            //Subscribe to Workspace PropertyChanged Event...
            ws.PropertyChanged += CurrentWorkspace_PropertyChanged;

            //Subscribe to Workspace Saving Events...
            ws.Saving += CurrentWorkspace_Saving;
            ws.Saved += CurrentWorkspace_Saved;

            //Subscribe to Miscellaneous Workspace Events...
            ws.CurrentOffsetChanged += CurrentWorkspace_CurrentOffsetChanged;
            ws.MessageLogged += CurrentWorkspace_MessageLogged;
            ws.RequestNodeCentered += CurrentWorkspace_RequestNodeCentered;
        }

        /// <summary>
        /// Unsubscibe to all workspace Events.
        /// </summary>
        /// <param name="ws">Current Workspace</param>
        public void UnsubsribeToAllWorkspaceEvents(WorkspaceModel ws)
        {
            //Subscribe to Workspace Annotation Events...
            ws.AnnotationAdded -= CurrentWorkspace_AnnotationAdded;
            ws.AnnotationRemoved -= CurrentWorkspace_AnnotationRemoved;
            ws.AnnotationsCleared -= CurrentWorkspace_AnnotationsCleared;

            //Subscribe to Workspace Node Events...
            ws.NodeAdded -= CurrentWorkspace_NodeAdded;
            ws.NodeRemoved -= CurrentWorkspace_NodeRemoved;
            ws.NodesCleared -= CurrentWorkspace_NodesCleared;

            //Subscribe to Workspace Note Events...
            ws.NoteAdded -= CurrentWorkspace_NoteAdded;
            ws.NoteRemoved -= CurrentWorkspace_NoteRemoved;
            ws.NotesCleared -= CurrentWorkspace_NotesCleared;

            //Subscribe to Workspace Connector Events...
            ws.ConnectorAdded -= CurrentWorkspace_ConnectorAdded;
            ws.ConnectorDeleted -= CurrentWorkspace_ConnectorDeleted;

            //Subscribe to Workspace PropertyChanged Event...
            ws.PropertyChanged -= CurrentWorkspace_PropertyChanged;

            //Subscribe to Workspace Saving Events...
            ws.Saving -= CurrentWorkspace_Saving;
            ws.Saved -= CurrentWorkspace_Saved;

            //Subscribe to Miscellaneous Workspace Events...
            ws.CurrentOffsetChanged -= CurrentWorkspace_CurrentOffsetChanged;
            ws.MessageLogged -= CurrentWorkspace_MessageLogged;
            ws.RequestNodeCentered -= CurrentWorkspace_RequestNodeCentered;
        }

        private void UpdateVMEventLog(string msg)
        {
            _VM.WorkspaceEventLog += "\nWorkspaceEvent : " + msg;
        }

        #endregion

        #region Event Handling

            #region DynamoModel Events

            // Do something when a workspace is opened...
            private void Model_WorkspaceAdded(WorkspaceModel ws)
            {
                //Store current workspace...
                _cws = ws;

                SubsribeToAllWorkspaceEvents(ws);

            }

            // Do something when a Workspace is removed...
            private void Model_WorkspaceRemoved(WorkspaceModel ws)
            {
                UnsubsribeToAllWorkspaceEvents(ws);
            }

            #endregion

            #region Workspace Events

                #region Annotation Events

                    private void CurrentWorkspace_AnnotationAdded(Dynamo.Graph.Annotations.AnnotationModel am)
                    {
                        UpdateVMEventLog("New Annotation Added");
                    }

                    private void CurrentWorkspace_AnnotationRemoved(Dynamo.Graph.Annotations.AnnotationModel am)
                    {
                        UpdateVMEventLog("Annotation Removed");
                    }

                    private void CurrentWorkspace_AnnotationsCleared()
                    {
                        UpdateVMEventLog("Annotations Cleared from Workspace");
                    }

                #endregion

                #region Node Events

                    private void CurrentWorkspace_NodeAdded(Dynamo.Graph.Nodes.NodeModel nm)
                    {
                        UpdateVMEventLog("New Node Added");
                    }

                    private void CurrentWorkspace_NodeRemoved(Dynamo.Graph.Nodes.NodeModel nm)
                    {
                        UpdateVMEventLog("Node Removed");
                    }

                    private void CurrentWorkspace_NodesCleared()
                    {
                        UpdateVMEventLog("Nodes Cleared from Workspace");
                    }

                #endregion

                #region Note Events

                    private void CurrentWorkspace_NoteAdded(Dynamo.Graph.Notes.NoteModel nm)
                    {
                        UpdateVMEventLog("New Note Added");
                    }

                    private void CurrentWorkspace_NoteRemoved(Dynamo.Graph.Notes.NoteModel nm)
                    {
                        UpdateVMEventLog("Note Removed");
                    }

                    private void CurrentWorkspace_NotesCleared()
                    {
                        UpdateVMEventLog("Notes Cleared from Workspace");
                    }

                #endregion

                #region Connector Events

                    private void CurrentWorkspace_ConnectorAdded(Dynamo.Graph.Connectors.ConnectorModel cm)
                    {
                        UpdateVMEventLog("New Connector Added");
                    }

                    private void CurrentWorkspace_ConnectorDeleted(Dynamo.Graph.Connectors.ConnectorModel cm)
                    {
                        UpdateVMEventLog("Connector Removed");
                    }

                #endregion

                #region PropertyChanged Events

                    private void CurrentWorkspace_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
                    {
                        UpdateVMEventLog("Workspace PropertyChanged : Property Name = " + e.PropertyName);
                    }

                #endregion

                #region Saving Events

                    private void CurrentWorkspace_Saving(System.Xml.XmlDocument obj)
                    {
                        UpdateVMEventLog("Workspace is Saving");
                    }

                    private void CurrentWorkspace_Saved()
                    {
                        UpdateVMEventLog("Workspace Saved");
                    }

                #endregion
    
                #region Miscellaneous Events

                    private void CurrentWorkspace_CurrentOffsetChanged(object sender, EventArgs e)
                    {
                        UpdateVMEventLog("Workspace Current Offset Changed");
                    }

                    private void CurrentWorkspace_MessageLogged(Dynamo.Logging.ILogMessage obj)
                    {
                        UpdateVMEventLog("Workspace Message Logged");
                    }

                    private void CurrentWorkspace_RequestNodeCentered(object sender, EventArgs e)
                    {
                        UpdateVMEventLog("Node Centered Requested");
                    }

                #endregion

            #endregion

        #endregion
    }
}
