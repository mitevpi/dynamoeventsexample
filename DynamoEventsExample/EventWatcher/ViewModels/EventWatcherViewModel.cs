﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Dynamo.Wpf.Extensions;

using DynamoEventsExample;

namespace DynamoEventsExample.EventWatcher.ViewModels
{
    class EventWatcherViewModel : INotifyPropertyChanged
    {

        #region Properties

        public static ViewLoadedParams _viewLoadedParams { get; set; }

        private WorkspaceEventsExample _wsEvents;

        private string _WorkspaceEventLog;

        public string WorkspaceEventLog
        {
            get { return _WorkspaceEventLog; }
            set
            {
                if (!string.IsNullOrEmpty(value) && value != _WorkspaceEventLog)
                {
                    _WorkspaceEventLog = value;
                    OnPropertyChanged("WorkspaceEventLog");
                }
            }
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Constructs a new static instance of the Event Watcher ViewModel.
        /// </summary>
        public EventWatcherViewModel()
        {
            ClearWorkspaceEventLog();
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject())) return;
            _wsEvents = new WorkspaceEventsExample(_viewLoadedParams, this);

            ClearLogsCommand = new RelayCommand(OnClearLog);
        }

        #endregion

        #region Methods

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        private void ClearWorkspaceEventLog()
        {
            WorkspaceEventLog = "Workspace Events:\n";
        }

        private void OnClearLog()
        {
            if (CanClearLog())
            {
                ClearWorkspaceEventLog();
            }            
        }

        public bool CanClearLog()
        {
            return true;
        }

        #endregion

        #region Commands

        public RelayCommand ClearLogsCommand { get; private set; }

        #endregion

    }

}
