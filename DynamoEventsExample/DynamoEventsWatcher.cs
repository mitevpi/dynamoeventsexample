﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Dynamo.Wpf.Extensions;

using DynamoEventsExample.EventWatcher.ViewModels;
using DynamoEventsExample.EventWatcher.Views;

namespace DynamoEventsExample
{
    public class DynamoEventsWatcher : IViewExtension
    {
        public string UniqueId
        {
            get
            {
                return Guid.NewGuid().ToString();
            }
        }

        public string Name
        {
            get
            {
                return "Dynamo Events Watcher";
            }
        }

        public void Startup(ViewStartupParams p)
        {

        }

        public void Loaded(ViewLoadedParams p)
        {
            EventWatcherViewModel._viewLoadedParams = p;

            Menu mainMenu = p.dynamoMenu;

            MenuItem _eventMainMenu = new MenuItem
            {
                Header = "Events Example VE"
            };
            mainMenu.Items.Add(_eventMainMenu);

            MenuItem _EventWatcher = new MenuItem
            {
                Header = "Launch Event Watcher"
            };
            _eventMainMenu.Items.Add(_EventWatcher);

            _EventWatcher.Click += (sender, args) =>
            {
                EventWatcherViewModel ewVM = new EventWatcherViewModel();
                var window = new EventWatcherView
                {
                    MainDockPanel = { DataContext = ewVM },
                    Owner = p.DynamoWindow
                };

                window.Left = window.Owner.Left + 500;
                window.Top = window.Owner.Top + 200;

                window.Show();

            };


            EventWatcherViewModel eventWatcherViewModel = new EventWatcherViewModel();
        }

        public void Dispose()
        {
            
        }

        public void Shutdown()
        {
            
        }
    }
}
